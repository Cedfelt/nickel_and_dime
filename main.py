# A Demo With some example usages

import transactions
import recivers
import matplotlib.pyplot as plt
import operator

def print_all_expenses_sorted(self, exceptions_list = []):
	classifications = recivers.get_all_classifications()
	exp_sum = 0
	for exc in exceptions_list:
		if exc in exceptions_list:
			classifications.remove(exc)
	for c in classifications:
		sub_grp = self.get_all_expenses_with_classification(c)
		if(len(sub_grp)!=0):
			categorie_sum = self.sum_of_all_expens_with_class(c)
			print("## " + c + ": " + str(categorie_sum))
			for exp in sub_grp:
				exp_sum += exp.amount
				print("    " + exp.dscrpt + " : " + str(exp.amount))
		
	sub_grp = self.get_all_expenses_with_classification("UNCLASSIFIED")
	if(len(sub_grp)!= 0):
		categorie_sum = self.sum_of_all_expens_with_class("UNCLASSIFIED")
		print("## UNCLASSIFIED" + ": " + str(categorie_sum))
		for exp in sub_grp:
			exp_sum += exp.amount
			print("    " + exp.dscrpt + " : " + str(exp.amount))

	print("## Total Expenses: " + str(exp_sum))

def print_all_incomes_sorted(self, exceptions_list = []):
	classifications = recivers.get_all_classifications()
	incomes = []
	exp_sum = 0
	# Remove Exceptions
	for exc in exceptions_list:
		if exc in exceptions_list:
			classifications.remove(exc)
		
	for c in classifications:
		sub_grp = self.get_all_incomes_with_classification(c)
		incomes += sub_grp
		if(len(sub_grp)!=0):
			categorie_sum = self.sum_of_all_incomes_with_class(c)
			print("## " + c + ": " + str(categorie_sum))
			for exp in sub_grp:
				exp_sum += exp.amount
				print("    " + exp.dscrpt + " : " + str(exp.amount))
		
	sub_grp = self.get_all_incomes_with_classification("UNCLASSIFIED")
	if(len(sub_grp)!= 0):
		categorie_sum = self.sum_of_all_incomes_with_class("UNCLASSIFIED")
		print("## UNCLASSIFIED" + ": " + str(categorie_sum))
		for exp in sub_grp:
			exp_sum += exp.amount
			print("    " + exp.dscrpt + " : " + str(exp.amount))

	print("## Total Incomes: " + str(exp_sum))
	return incomes,exp_sum

def show_expenses_as_pie_chart(self, exceptions_list = []):
	classifications = recivers.get_all_classifications()
	exp_sum = 0
	grp_sums = []
	class_list = []
	result = {}
	for exc in exceptions_list:
		if exc in exceptions_list:
			classifications.remove(exc)
	for c in classifications:
		sub_grp = self.get_all_expenses_with_classification(c)
		if(len(sub_grp)!=0):
			
			categorie_sum = self.sum_of_all_expens_with_class(c)
			grp_sums.append(categorie_sum)
			class_list.append(c)
			result[c] = categorie_sum
			
			for exp in sub_grp:
				exp_sum += exp.amount
				
		
	sub_grp = self.get_all_expenses_with_classification("UNCLASSIFIED")
	if(len(sub_grp)!= 0):
		categorie_sum = self.sum_of_all_expens_with_class("UNCLASSIFIED")
		
		for exp in sub_grp:
			exp_sum += exp.amount
			

	# Data to plot
	
	sizes = grp_sums
	labels =  class_list
	sizes =  [abs(ele) for ele in sizes]
	
	combined_list = zip(labels,sizes)
	combined_list = sorted(combined_list, key = lambda t: t[1])

	labels,sizes = list(zip(*combined_list))

	colors = ['violet', 'lightcoral','darkkhaki','plum', 'turquoise','mintcream','azure','lightgrey','palegreen','skyblue','paleturquoise','gold','yellowgreen',  'lightskyblue']
	
	

	# Plot
	plt.pie(sizes, labels=labels,autopct='%1.1f%%', shadow=True, startangle=90,colors = colors)
	plt.axis('equal')
	plt.title("Total: "+str(exp_sum) +" kr",loc='left')
	plt.savefig("test.jpeg")
	plt.show()

# Asa Konto
transaction_period_asa = transactions.TransactionPeriod("Transaktioner_2020-11-02_00-12-02.csv")

# Anders Konto
transaction_period_anders = transactions.TransactionPeriod("Transaktioner_2020-11-01_10-32-47.csv")
transaction_period_anders.merge_periods(transaction_period_asa)

transaction_period_anders.remove_transaction('2020-10-05', 'Macforum AB', -5900.0)
transaction_period_anders.remove_transaction('2020-10-27', 'STUDIO STRANDH', -10305.00)  



print("\n# Incomes")
incomes,exp_sum = print_all_incomes_sorted(transaction_period_anders,exceptions_list=["Savings_Withdrawal","Internal_Transactions"])

print("\n# Expenses")
print_all_expenses_sorted(transaction_period_anders,exceptions_list=["Savings","Internal_Transactions"])


show_expenses_as_pie_chart(transaction_period_anders,exceptions_list=["Internal_Transactions"])

