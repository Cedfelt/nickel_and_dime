import csv
import recivers


class Transaction(object):
	"""docstring for ClassName"""
	def __init__(self, csv_row):
		self.row_num = csv_row[0]
		self.clr_num = csv_row[1]
		self.act_num = csv_row[2]
		self.prd = csv_row[3]
		self.cur = csv_row[4]
		self.bok_day = csv_row[5]
		self.trn_day = csv_row[6]
		self.cur_day = csv_row[7]
		self.ref = csv_row[8]
		self.dscrpt = csv_row[9]
		self.amount = float(csv_row[10]) 
		self.balance = float(csv_row[11])
		self.classify_transaction()

	def classify_transaction(self):
		if self.dscrpt in recivers.recivers:
			self.classification = recivers.recivers[self.dscrpt]
		else:
			self.classification = "UNCLASSIFIED"



class TransactionPeriod(object):
	"""docstring for TransactionPeriod"""
	def __init__(self,file_name):
		self.transactions = []
		with open(file_name) as File:
			reader = csv.reader(File)
			for cnt,row in enumerate(reader):
				if(cnt==0):
					self.meta_data = row
					continue

				if(cnt==1):
					continue
				self.add_transaction(row)
	
	def merge_periods(self,period_to_merge):
		self.transactions += period_to_merge.transactions
	def remove_transaction(self,bok_day,dscrpt,amount):
		for t in self.transactions:
			if((t.bok_day == bok_day) and (t.dscrpt == dscrpt) and (t.amount == amount)):
				self.transactions.remove(t)

	def add_transaction(self,row):
		self.transactions.append(Transaction(row))

	def get_all_expenses(self):
		expenses = []
		for trans in self.transactions:
			if(trans.amount < 0):
				expenses.append(trans)

		return expenses


	def get_all_unclassified_expenses(self):
		expenses = []
		for trans in self.transactions:
			if(trans.amount < 0 and trans.classification == "UNCLASSIFIED"): 
				expenses.append(trans)

		return expenses

	def sum_of_all_expens_with_class(self,classification):
		expenses = []
		for trans in self.transactions:
			if(trans.amount < 0 and trans.classification == classification): 
				expenses.append(trans)
		exp_sum = 0
		for e in expenses:
			exp_sum+=e.amount
		return exp_sum

	def sum_of_all_incomes_with_class(self,classification):
		incomes = []
		for trans in self.transactions:
			if(trans.amount >= 0 and trans.classification == classification): 
				incomes.append(trans)
		exp_sum = 0
		for e in incomes:
			exp_sum+=e.amount
		return exp_sum


	def get_all_expenses_with_classification(self,classification):
		expenses = []
		for trans in self.transactions:
			if((trans.amount < 0) and (trans.classification == classification)):
				expenses.append(trans)

		return expenses

	def get_all_incomes_with_classification(self,classification):
		incomes = []
		for trans in self.transactions:
			if((trans.amount > 0) and (trans.classification == classification)):
				incomes.append(trans)

		return incomes